#!/bin/bash
echo "Please specify Commit message!"
read commitmsg
if [[ $commitmsg != "" ]]; then
	git add .
	git commit -m "$commitmsg"
	git push origin master
else
	echo "ERROR, Please provide a Commit message!"
fi