#Git quick add,comit,push

##Why?

Just because I want to develop my apps faster and not waisting time by typing something like

`git add .`

`git commit -m "Message"`

`git push origin master`

This is my small bash script to excecute the following command squence and speed up my git workflow on the master branch.

To run this script globally for **Current User** (I am talking about **Mac users**)

You do this by creating a *link* to a file.sh (in our case it is gq.sh) that sits in your specified directory, and you link it to a path where you store your scripts, executables to be run globally.

**Note!** Please do **NOT** use to `/usr/bin` for obvious reasons

###Command as follows

`sudo ln -s /Users/user/path/to/file /usr/local/bin/gq(prefered :))`

and of course don't forget

`chmod +x gs.sh` to actually execute it (you need to be in a current directory)

So to run my script I need to make this to run as an executable by simply typying in a comman `gq` that will prompt me to enter a Commit message and will push *all the files that have been changed* to the repository.

**Additionally** instead of having a copy of this script in your specified directory, just place it in your /usr/local/bin/ and name it the way you want, that also works, if you don't want to create a link.


And also I have a `ginit` shell script that will set up a current directory as a git directory

Can be found here [GInit](https://bitbucket.org/n1ghtt/ginit)

#TO-DO
+ Make it promt for files to be commited and have a corresponding commit message attached to a specific file
+ Better and more excplicit validation
+ More ideas will come and more fun with shell scripting! :)
